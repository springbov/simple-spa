const history = window.history
const baseURL = "/#/";
let contentElement;

/**
 * @typedef {State}
 * @type {object}
 * @property {boolean} default is it the default state?
 * @property {string} url the way to id the state
 * @property {string} htmlUrl The url to fetch the html to put into the new state 
 * @property {string} html The html to put into the new state
 * @property {string} title the title to put into the new state
 * @property {function} init function to run after loading the html
 * @property {function} destroy function to run before loading the next state
 */

/** @type {State[]} */
const states = [];

/** @type {State} */
let currentState;

function setStates(moreStates)
{
  moreStates.forEach(s => {
    checkIfValidState(s);
    states.push(s)
  });
}

function checkIfValidState(state)
{
  let valid = state && typeof state.url == "string";

  if (valid)
  {
    if (states.filter(s => s.url == state.url).length) // this is before it's added
    {
      throw new Error(`duplicate URL: {state.url}`);
    }
  }
  else
  {
    throw new Error(`${JSON.stringify(state)} : is invalid`);
  }
}

function setContentElement(name)
{
  contentElement = name;
}

function url(s)
{
  return baseURL + s
}

function onWindowLoad()
{
  if (!contentElement)
  {
    contentElement = document.getElementById("simple-spa-content")
  }
  let availState = getStateFromURL(getEndyPartOfURL());
  if (availState)
  {
    loadState(availState,  getParamsFromURL());
    history.replaceState(availState.url, availState.title, url(getEndyPartOfURL()));
  }
  else
  {
    // if not, well that sucks, load the default state
    let defaultList = states.filter(s => s.default);
    if (defaultList.length > 0)
    {
      let defaultState = defaultList[0];
      loadState();
      history.replaceState(defaultState.url, defaultState.title, url(defaultState.url ? defaultState.url : ""));
    }
  }
}

function getEndyPartOfURL()
{
  let origin = window.location.origin;
  let urlPart = window.location.href.substring(origin.length + baseURL.length);
  return urlPart;
}


/**
 * Get's the state from the url part.
 * The url part may contain a question mark and have params after it
 * @param {string} urlPart the portion after the base url, so the <domain>/urlPart?asdf
 */
function getStateFromURL(urlPart)
{
  if (urlPart)
  {
    let qmarkDex = urlPart.indexOf("?");
    let withoutParams = urlPart.substring(0, qmarkDex > -1 ? qmarkDex : urlPart.length); 
    let filtered = states.filter(s => s.url == withoutParams);
    if (filtered.length) return filtered[0];
    else return null;
  }
  return null;
}

/**
 * @parameter {State | string} state a string of the url or a state object
 * @returns {boolean} true if the state exists false if it goes to default
 */
function goTo(state, params)
{
  const go = (s) => {
    loadState(s, params)
    let histUrl = s.url;
    if (params) histUrl = `${histUrl}?${params}`;
    history.pushState(s.url, s.title, url(histUrl));
  }
  let validState = false;
  if (typeof state == "string")
  {
    // lookup the state
    let lookup = states.filter(s => s.url == state);
    validState = lookup.length > 0;
    if (validState)
    {
      go(lookup[0]);
    }
  }
  else if (typeof state == "object")
  {
    go(state);
    validState = true;
  }
  return validState;
}

/**
 * Loads a state
 */
function loadState(state, params)
{
  const setupPage = (html) => {
    if (currentState && currentState.destroy) currentState.destroy()
    if (html)
    {
      state.html = html
    }
    contentElement.innerHTML = state.html;
    if (state.init) state.init(parseParamsString(params))
    currentState = state;
  };
  if (state && !state.html && state.htmlUrl)
  {
    fetchHtml(state.htmlUrl, setupPage);
  }
  else if(state && state.html)
  {
    setupPage()
  }
}

/**
 * parses an html part and returns an object of params.
 */
function getParamsFromURL()
{
  let urlPart = getEndyPartOfURL();

  let questionIndex = urlPart.indexOf("?");
  let params = ""
  if (questionIndex > -1)
  {
    params = urlPart.substring(questionIndex + 1);
  }
  return params;
}

function parseParamsString(paramsStr)
{
  let paramsJect = {};
  if (paramsStr)
  {
    const parsePair = (pairstr) =>
    {
      let pair = undefined;
      let eqDex = pairstr.indexOf("=");
      if (eqDex > -1)
      {
        pair = [pairstr.substring(0, eqDex), pairstr.substring(eqDex + 1)];
      }
      return pair;
    }

    let pairsArry = paramsStr.split("&");

    for (let pairStr of pairsArry)
    {
      let pair = parsePair(pairStr);
      if (pair) paramsJect[pair[0]] = pair[1];
    }
  }

  return paramsJect;
}

function fetchHtml(url, callback)
{
  fetch(url)
    .then(r => r.text())
    .then(callback)
}

window.addEventListener("popstate", onWindowLoad);
window.addEventListener("hashchange", onWindowLoad);

export default {
  setStates: setStates,
  setContentElement: setContentElement,
  goTo: goTo,
  onLoad: onWindowLoad
}
