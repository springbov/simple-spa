import simpleSpa from "./src/simple-spa.js";

let states = [
  {
    default: true,
    url: "",
    htmlUrl: "default.html",
    title: "zippidy do da",
    init: () => console.log("guess what terd biscuit?"),
    destroy: () => console.log("thats what I thought")
  },
  {
    url: "takotime",
    html: "<p>inline html is cool too i guess</p>",
    title: "zippidy do da",
    init: () => console.log("so, takotime, am I right?"),
    destroy: () => console.log("nope, you're wrong again.")
  },
  {
    url: "francois",
    html: `
    <p id="ptag"></p>
    `,
    title: "ugly barnacle",
    init: (params) => {
      console.log("params on init", params);
      const thing = document.getElementById("ptag");
      thing.innerText = `ahoy ${JSON.stringify(params)}`;
    },
  }
];

simpleSpa.setStates(states);
simpleSpa.onLoad();
let tako = document.getElementById("gotoTakoTime");
tako.addEventListener("click", () => simpleSpa.goTo("takotime"))
let btn1 = document.getElementById("gotofrancois");
let btn2 = document.getElementById("gotofrancois2");
btn1.addEventListener("click", () => simpleSpa.goTo("francois", "dark=fart&tako=seasoning"));
btn2.addEventListener("click", () => simpleSpa.goTo("francois"));
